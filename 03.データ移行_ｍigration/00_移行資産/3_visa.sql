TRUNCATE TABLE visa;
INSERT INTO visa(
person_id
,end_date
,residence_card_no
)
SELECT
student.person_id
,sys8_visa.end_date
,system8_1_student_base.`在留カード番号`
FROM system8_1_student_base
INNER JOIN student ON student.student_no=system8_1_student_base.`学籍番号`
LEFT JOIN (
	SELECT
		`学籍番号`
		,`VISA更新日(1)` as end_date
	FROM system8_1_student_base as sys8_visa1
	UNION
	SELECT
		`学籍番号`
		,`VISA更新日(2)` as end_date
	FROM system8_1_student_base as sys8_visa2
	UNION
	SELECT
		`学籍番号`
		,`VISA更新日(3)` as end_date
	FROM system8_1_student_base as sys8_visa3
	UNION
	SELECT
		`学籍番号`
		,`VISA更新日(4)` as end_date
	FROM system8_1_student_base as sys8_visa4
)sys8_visa ON sys8_visa.`学籍番号`=system8_1_student_base.`学籍番号`
WHERE true
AND NOT ( true
AND sys8_visa.end_date =''
)
AND NOT ( true
AND sys8_visa.end_date ='00000000'
)
ORDER BY student.person_id,sys8_visa.end_date
;

-- ------------------------------------------------------------------------------
-- visa.start_dateの設定
-- ------------------------------------------------------------------------------
SELECT visa.id,visa_work.id,visa.person_id,visa.end_date,visa_work.end_date
FROM visa
INNER JOIN visa visa_work ON visa_work.person_id=visa.person_id
WHERE visa.end_date>visa_work.end_date AND visa.id=(visa_work.id+1)
;

UPDATE visa
INNER JOIN visa visa_work ON visa_work.person_id=visa.person_id
SET visa.start_date=ADDDATE(visa_work.end_date, 1)
WHERE visa.end_date>visa_work.end_date AND visa.id=(visa_work.id+1)
;

-- ------------------------------------------------------------------------------
-- visa.start_dateの設定(1回目)
-- ------------------------------------------------------------------------------
SELECT * FROM visa
WHERE start_date='0000-00-00'
;

UPDATE visa
INNER JOIN student ON student.person_id=visa.person_id
INNER JOIN system8_1_student_base ON system8_1_student_base.`学籍番号`=student.student_no
SET visa.start_date=system8_1_student_base.`入国年月日`
,visa.lastup_account_id=2000
,visa.lastup_datetime=now()
WHERE visa.start_date='0000-00-00'
;



-- ------------------------------------------------------------------------------
-- visa.start_dateの設定(1回目)
-- ------------------------------------------------------------------------------
SELECT 
length_of_stay.* 
,CASE
	WHEN year=0 AND month=0 THEN ''
	WHEN year>0 AND month>0 THEN CONCAT(year,'年',month,'月')
	WHEN year>0 AND month=0 THEN CONCAT(year,'年')
	WHEN year=0 AND month>0 THEN CONCAT(month,'月')
	ELSE ''
END as length
FROM(
SELECT visa.*
,CASE 
	WHEN TIMESTAMPDIFF(MONTH,start_date,DATE_ADD(end_date,INTERVAL 1 DAY)) >=12 THEN TIMESTAMPDIFF(MONTH,start_date,DATE_ADD(end_date,INTERVAL 1 DAY)) DIV 12 
	ELSE 0
END as year
,CASE 
	WHEN TIMESTAMPDIFF(MONTH,start_date,DATE_ADD(end_date,INTERVAL 1 DAY)) >=12 THEN TIMESTAMPDIFF(MONTH,start_date,DATE_ADD(end_date,INTERVAL 1 DAY)) % 12 
	ELSE TIMESTAMPDIFF(MONTH,start_date,DATE_ADD(end_date,INTERVAL 1 DAY))
END as month
FROM visa
)length_of_stay
;

UPDATE visa
LEFT JOIN (
SELECT 
visa_id
,CASE
	WHEN year=0 AND month=0 THEN ''
	WHEN year>0 AND month>0 THEN CONCAT(year,'年',month,'月')
	WHEN year>0 AND month=0 THEN CONCAT(year,'年')
	WHEN year=0 AND month>0 THEN CONCAT(month,'月')
	ELSE ''
END as length
FROM(
SELECT visa.id as visa_id
,CASE 
	WHEN TIMESTAMPDIFF(MONTH,start_date,DATE_ADD(end_date,INTERVAL 1 DAY)) >=12 THEN TIMESTAMPDIFF(MONTH,start_date,DATE_ADD(end_date,INTERVAL 1 DAY)) DIV 12 
	ELSE 0
END as year
,CASE 
	WHEN TIMESTAMPDIFF(MONTH,start_date,DATE_ADD(end_date,INTERVAL 1 DAY)) >=12 THEN TIMESTAMPDIFF(MONTH,start_date,DATE_ADD(end_date,INTERVAL 1 DAY)) % 12 
	ELSE TIMESTAMPDIFF(MONTH,start_date,DATE_ADD(end_date,INTERVAL 1 DAY))
END as month
FROM visa
)length_of_stay
) aaa ON aaa.visa_id=visa.id
SET visa.visa_length_of_stay=aaa.length
;

UPDATE visa SET visa_length_of_stay= REPLACE(visa_length_of_stay, '0','０');
UPDATE visa SET visa_length_of_stay= REPLACE(visa_length_of_stay, '1','１');
UPDATE visa SET visa_length_of_stay= REPLACE(visa_length_of_stay, '2','２');
UPDATE visa SET visa_length_of_stay= REPLACE(visa_length_of_stay, '3','３');
UPDATE visa SET visa_length_of_stay= REPLACE(visa_length_of_stay, '4','４');
UPDATE visa SET visa_length_of_stay= REPLACE(visa_length_of_stay, '5','５');
UPDATE visa SET visa_length_of_stay= REPLACE(visa_length_of_stay, '6','６');
UPDATE visa SET visa_length_of_stay= REPLACE(visa_length_of_stay, '7','７');
UPDATE visa SET visa_length_of_stay= REPLACE(visa_length_of_stay, '8','８');
UPDATE visa SET visa_length_of_stay= REPLACE(visa_length_of_stay, '9','９');

-- ------------------------------------------------------------------------------------------------------------
-- check
-- ------------------------------------------------------------------------------------------------------------
SELECT
	student.student_no as `学籍番号`
	,student.person_id
	,`入国年月日`
	,`VISA更新日(1)`
	,`VISA更新日(2)`
	,`VISA更新日(3)`
	,`VISA更新日(4)`
	,`在留カード番号`
FROM student
LEFT JOIN system8_1_student_base ON student.student_no=system8_1_student_base.`学籍番号`
ORDER BY student.person_id
;

SET @rownum:=0;
SET @person_id:=null;
SELECT
student.person_id
,`VISA発行日(1)OR入国年月日`
,`VISA更新日(1)`
,`在留カード番号(1)`
,`VISA発行日(2)`
,`VISA更新日(2)`
,`在留カード番号(2)`
,`VISA発行日(3)`
,`VISA更新日(3)`
,`在留カード番号(3)`
,`VISA発行日(4)`
,`VISA更新日(4)`
,`在留カード番号(4)`
FROM student
LEFT JOIN (
	SELECT
		person_id
		,max(case rownum when 1 then start_date else null end) as `VISA発行日(1)OR入国年月日`
		,max(case rownum when 1 then end_date else null end) as `VISA更新日(1)`
		,max(case rownum when 1 then residence_card_no else null end) as `在留カード番号(1)`
		,max(case rownum when 2 then start_date else null end) as `VISA発行日(2)`
		,max(case rownum when 2 then end_date else null end) as `VISA更新日(2)`
		,max(case rownum when 2 then residence_card_no else null end) as `在留カード番号(2)`
		,max(case rownum when 3 then start_date else null end) as `VISA発行日(3)`
		,max(case rownum when 3 then end_date else null end) as `VISA更新日(3)`
		,max(case rownum when 3 then residence_card_no else null end) as `在留カード番号(3)`
		,max(case rownum when 4 then start_date else null end) as `VISA発行日(4)`
		,max(case rownum when 4 then end_date else null end) as `VISA更新日(4)`
		,max(case rownum when 4 then residence_card_no else null end) as `在留カード番号(4)`
	FROM (
		SELECT 
			if(@person_id<> person_id, @rownum:=1, @rownum:=@rownum+1) as rownum
			,@person_id:=person_id as person_id
			,start_date
			,end_date
			,residence_card_no
		FROM visa
		ORDER BY person_id,start_date
	)TEMP
	GROUP BY person_id
)VISA ON VISA.person_id=student.person_id
ORDER BY student.person_id
;