-- ------------------------------------------------------------------------------
-- 手順①－１　新入生の半角スペースを全角スペースへ置換　system8
-- ------------------------------------------------------------------------------
update system8_1_student_base
set 本国氏名1 = REPLACE(本国氏名1 ,' ','　'),
本国氏名2 = REPLACE(本国氏名2 ,' ','　'),
フリガナ名 = REPLACE(フリガナ名 ,' ','　') 
where 学籍番号 in (
(SELECT 
KIHON.student_no as student_no
FROM (
	SELECT
		`学籍番号` as student_no
		,`登録番号` 
		,`本国氏名書体` as name_font
		,`本国氏名1` as name
		,`本国氏名2` as name_en
		,`フリガナ名` as name_kana
		,`性別` as sex
		,`未既婚` as married
		,`国名` 
		,`生年月日` as birthday
		,`紹介者名` 
		,`本国住所書体` 
		,`本国住所` 
		,`本国電話` 
		,`旅券番号` 
		,`旅券有効期限` 
		,`入国年月日` 
		,`生VISA種類` 
		,`VISA更新日(1)` 
		,`VISA更新日(2)` 
		,`VISA更新日(3)` 
		,`VISA更新日(4)` 
		,`入管許可番号` 
		,`入管許可日` 
		,`在留カード番号` 
		,`入学日` as start_date
		,`卒修予定日` as end_date_intended
		,`コース名` as course
		,`クラス名` 
		,`退学年月日` as end_date1
		,`除籍年月日` as end_date2
		,`退学除籍理由` as reason_for_exclusion
		,`在籍卒業区分` as status_name
		,`以前住所〒` as previous_address_zipcode
		,`以前住所住` as previous_address1
		,`現住所〒` as zipcode
		,`現住所住` as address1
		,`現住所電話` as phone_no
		,`携　帯電話` as mobile_no
		,`国民健康保険番号` 
		,`障害保険有無` 
		,`母国語` as native_language
		,`卒業後進路先` as after_graduate_school
		,`紹介者書体` 
		,`紹介者名1` 
		,`紹介者名2` 
		,`紹介者住所1` 
		,`紹介者住所2` 
		,`紹介者TEL` 
		,`紹介者FAX` 
		,`J   ﾃｽﾄ` 
		,`NAT ﾃｽﾄ` 
		,`入学ﾃｽﾄ` 
		,`組分ﾃｽﾄ` 
	FROM system8_1_student_base
)KIHON
LEFT JOIN student ON student.student_no=KIHON.student_no
WHERE student.student_no IS NULL
))
-- ------------------------------------------------------------------------------
-- 手順①－２　system8削除対象電話番号を一括置換「03-3368-4931」学校の電話番号を仮設定しているデータが多く存在する
-- ------------------------------------------------------------------------------
UPDATE system8_1_student_base
SET `本国電話` = '' 
WHERE `本国電話` = '03-3368-4931';

UPDATE system8_1_student_base
SET `現住所電話` = '' 
WHERE `現住所電話` = '03-3368-4931';

UPDATE system8_1_student_base
SET `携　帯電話` = '' 
WHERE `携　帯電話` = '03-3368-4931';

-- ------------------------------------------------------------------------------
-- 手順②　新入生データをINSERT
-- ------------------------------------------------------------------------------
INSERT INTO person(
type
-- ,country_id
,native_language
-- ,parent_person_id
,birthday
,sex_cd
,married_cd
,zipcode
,address_country_id
,address1
-- ,address2
-- ,address3
,phone_no
-- ,phone_no2
,mobile_no
,previous_address_zipcode
,previous_address1
-- ,previous_address2
-- ,previous_address3
-- ,mail_address1
-- ,mail_address2
,last_name
,first_name
,name_font
,last_name_kana
,first_name_kana
,last_name_alphabet
,first_name_alphabet
-- ,legal_registered_last_name
-- ,legal_registered_first_name
-- ,legal_registered_last_name_kana
-- ,legal_registered_first_name_kana
-- ,legal_registered_last_name_alphabet
-- ,legal_registered_first_name_alphabet
-- ,abbreviation_name
-- ,last_name_native -- 姓_母国語
-- ,first_name_native -- 名_母国語
,lastup_account_id
)
SELECT 
1 as type -- 1:学生
,KIHON.native_language
,KIHON.birthday
,CASE KIHON.sex
	WHEN '男' THEN 1
	WHEN '女' THEN 2
	ELSE 0
END as sex_cd
,CASE KIHON.married
	WHEN '未婚' THEN 1
	WHEN '既婚' THEN 2
	ELSE 0
END as married_cd
,KIHON.zipcode
,'156' as address_country_id -- 156:日本
,KIHON.address1
,CASE KIHON.phone_no
	WHEN '無' THEN ''
	WHEN 'なし' THEN ''
	ELSE KIHON.phone_no
END as phone_no
,CASE KIHON.mobile_no
	WHEN '無' THEN ''
	WHEN 'なし' THEN ''
	ELSE KIHON.mobile_no
END as mobile_no
,KIHON.previous_address_zipcode
,KIHON.previous_address1
,SUBSTRING_INDEX(KIHON.name, '　', 1) as last_name
,TRIM(LEADING '　' FROM REPLACE(KIHON.name,SUBSTRING_INDEX(KIHON.name, '　', 1),'')) as first_name
,KIHON.name_font
,SUBSTRING_INDEX(KIHON.name_kana, '　', 1) as last_name_kana
,TRIM(LEADING '　' FROM REPLACE(KIHON.name_kana,SUBSTRING_INDEX(KIHON.name_kana, '　', 1),'')) as first_name_kana
,SUBSTRING_INDEX(KIHON.name_en, '　', 1) as last_name_alphabet
,TRIM(LEADING '　' FROM REPLACE(KIHON.name_en,SUBSTRING_INDEX(KIHON.name_en, '　', 1),'')) as first_name_alphabet
,KIHON.student_no as lastup_account_id
FROM (
	SELECT
		`学籍番号` as student_no
		,`登録番号` 
		,`本国氏名書体` as name_font
		,`本国氏名1` as name
		,`本国氏名2` as name_en
		,`フリガナ名` as name_kana
		,`性別` as sex
		,`未既婚` as married
		,`国名` 
		,`生年月日` as birthday
		,`紹介者名` 
		,`本国住所書体` 
		,`本国住所` 
		,`本国電話` 
		,`旅券番号` 
		,`旅券有効期限` 
		,`入国年月日` 
		,`生VISA種類` 
		,`VISA更新日(1)` 
		,`VISA更新日(2)` 
		,`VISA更新日(3)` 
		,`VISA更新日(4)` 
		,`入管許可番号` 
		,`入管許可日` 
		,`在留カード番号` 
		,`入学日` as start_date
		,`卒修予定日` as end_date_intended
		,`コース名` as course
		,`クラス名` 
		,`退学年月日` as end_date1
		,`除籍年月日` as end_date2
		,`退学除籍理由` as reason_for_exclusion
		,`在籍卒業区分` as status_name
		,`以前住所〒` as previous_address_zipcode
		,`以前住所住` as previous_address1
		,`現住所〒` as zipcode
		,`現住所住` as address1
		,`現住所電話` as phone_no
		,`携　帯電話` as mobile_no
		,`国民健康保険番号` 
		,`障害保険有無` 
		,`母国語` as native_language
		,`卒業後進路先` as after_graduate_school
		,`紹介者書体` 
		,`紹介者名1` 
		,`紹介者名2` 
		,`紹介者住所1` 
		,`紹介者住所2` 
		,`紹介者TEL` 
		,`紹介者FAX` 
		,`J   ﾃｽﾄ` 
		,`NAT ﾃｽﾄ` 
		,`入学ﾃｽﾄ` 
		,`組分ﾃｽﾄ` 
	FROM system8_1_student_base
)KIHON
LEFT JOIN student ON student.student_no=KIHON.student_no
WHERE student.student_no IS NULL
;



-- ------------------------------------------------------------------------------
-- 手順③　新入生--名前がズレてないか確認
-- ------------------------------------------------------------------------------
SELECT *
FROM `person`
WHERE `lastup_account_id` IN 
(1810001,1810002,1810003,1810004,1810005,1810006,1810007,1810008,1810009,1810010,1810011,1810012,1810013,1810014,1810015,1810016,1810017,1810018,1810019,1810020,1810021,1810022,1810023,1810024,1810025,1810026,1810027,1810028,1810029,1810030,1810031,1810032,1810033,1810034,1810035,1810036,1810037,1810038,1810039,1810040,1810041,1810042,1810043,1810044,1810045,1810046,1810047,1810048,1810049,1810050,1810051,1810052,1810053,1810054,1810055,1810056,1810057,1810058,1810059,1810060,1810061,1810062,1810063,1810064,1810065,1810066,1810067,1810068,1810069,1810070,1810071,1810073,1810074,1810075,1810076,1810077,1810078,1810079,1810080,1810081,1810082,1810083,1810084,1810085,1810086,1810087,1810088,1810089,1810090,1810091,1810092,1810093,1810094,1810095,1810096,1810097,1810098,1810099,1810100,9918011,9918012)

-- ------------------------------------------------------------------------------
-- 手順④　在校生UPDATE
-- ------------------------------------------------------------------------------
UPDATE person
LEFT JOIN student ON student.person_id=person.id
LEFT JOIN (
	SELECT
		`学籍番号` as student_no
		,`登録番号` 
		,`本国氏名書体` as name_font
		,`本国氏名1` as name
		,`本国氏名2` as name_en
		,`フリガナ名` as name_kana
		,`性別` as sex
		,`未既婚` as married
		,`国名` 
		,`生年月日`
		,`紹介者名` 
		,`本国住所書体` 
		,`本国住所` 
		,`本国電話` 
		,`旅券番号` 
		,`旅券有効期限` 
		,`入国年月日` 
		,`生VISA種類` 
		,`VISA更新日(1)` 
		,`VISA更新日(2)` 
		,`VISA更新日(3)` 
		,`VISA更新日(4)` 
		,`入管許可番号` 
		,`入管許可日` 
		,`在留カード番号` 
		,`入学日` as start_date
		,`卒修予定日` as end_date_intended
		,`コース名` as course
		,`クラス名` 
		,`退学年月日` as end_date1
		,`除籍年月日` as end_date2
		,`退学除籍理由` as reason_for_exclusion
		,`在籍卒業区分` as status_name
		,`以前住所〒` as previous_address_zipcode
		,`以前住所住` as previous_address1
		,`現住所〒` as zipcode
		,`現住所住` as address1
		,`現住所電話` as phone_no
		,`携　帯電話` as mobile_no
		,`国民健康保険番号` 
		,`障害保険有無` 
		,`母国語` as native_language
		,`卒業後進路先` as after_graduate_school
		,`紹介者書体` 
		,`紹介者名1` 
		,`紹介者名2` 
		,`紹介者住所1` 
		,`紹介者住所2` 
		,`紹介者TEL` 
		,`紹介者FAX` 
		,`J   ﾃｽﾄ` 
		,`NAT ﾃｽﾄ` 
		,`入学ﾃｽﾄ` 
		,`組分ﾃｽﾄ` 
	FROM system8_1_student_base
)KIHON ON KIHON.student_no=student.student_no
SET 
-- person.type
-- person.country_id
person.native_language=KIHON.native_language
-- ,person.parent_person_id
,person.birthday=KIHON.`生年月日`
,person.sex_cd=	CASE KIHON.sex
					WHEN '男' THEN 1
					WHEN '女' THEN 2
					ELSE 0
				END
,person.married_cd=	CASE KIHON.married
						WHEN '未婚' THEN 1
						WHEN '既婚' THEN 2
						ELSE 0
					END
,person.zipcode=KIHON.zipcode
,person.address_country_id='156' -- 156:日本
,person.address1=KIHON.address1
-- ,person.address2
-- ,person.address3
-- ,person.phone_no= 
-- CASE KIHON.phone_no
--	WHEN '無' THEN ''
--	WHEN 'なし' THEN ''
--	ELSE KIHON.phone_no
-- END
-- ,person.phone_no2 
-- ,person.mobile_no=
-- CASE KIHON.mobile_no
--	WHEN '無' THEN ''
--	WHEN 'なし' THEN ''
--	ELSE KIHON.mobile_no
-- END
,person.previous_address_zipcode=KIHON.previous_address_zipcode
,person.previous_address1=KIHON.previous_address1
-- ,person.previous_address2
-- ,person.previous_address3
-- ,person.mail_address1
-- ,person.mail_address2
-- ,person.last_name -- 名前（漢字、英字、読み方）は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.first_name -- 名前（漢字、英字、読み方）は新システムをそのまま残して、それ以外の項目を上書きする
,person.name_font=KIHON.name_font
-- ,person.last_name_kana -- 名前（漢字、英字、読み方）は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.first_name_kana -- 名前（漢字、英字、読み方）は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.last_name_alphabet -- 名前（漢字、英字、読み方）は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.first_name_alphabet -- 名前（漢字、英字、読み方）は新システムをそのまま残して、それ以外の項目を上書きする
-- ,person.legal_registered_last_name -- 戸籍
-- ,person.legal_registered_first_name -- 戸籍
-- ,person.legal_registered_last_name_kana -- 戸籍
-- ,person.legal_registered_first_name_kana -- 戸籍
-- ,person.legal_registered_last_name_alphabet -- 戸籍
-- ,person.legal_registered_first_name_alphabet -- 戸籍
-- ,person.abbreviation_name -- 表示用
-- ,person.last_name_native -- 姓_母国語
-- ,person.first_name_native -- 名_母国語
-- ,person.lastup_account_id=2000
-- ,person.lastup_datetime=now()
;



-- ------------------------------------------------------------------------------
-- 手順⑤　国名をUPDATE system8に登録されている国名は、固定値ではないため
--　　　　毎回確認が必要⇒　SELECT  DISTINCT(`国名`) FROM `system8_1_student_base`   ORDER BY '国名';
-- ------------------------------------------------------------------------------
UPDATE person
LEFT JOIN student ON student.person_id=person.id
LEFT JOIN system8_1_student_base ON system8_1_student_base.`学籍番号`=student.student_no
LEFT JOIN country ON country.name=system8_1_student_base.`国名` AND country.disable = 0
SET person.country_id=	CASE system8_1_student_base.`国名`
				WHEN 'アルジェリア' THEN 9
				WHEN 'インド' THEN 26
				WHEN 'インドネシア' THEN 27
				WHEN 'ウズベキスタン' THEN 31
				WHEN 'オーストラリア' THEN 39
				WHEN 'スリナム' THEN 108
				WHEN 'スリランカ' THEN 109
				WHEN 'タイ' THEN 125
				WHEN 'チリ' THEN 136
				WHEN 'ドイツ' THEN 139
				WHEN 'トルコ' THEN 146
				WHEN 'ネパール' THEN 160
				WHEN 'パキスタン' THEN 167
				WHEN 'バングラデシュ' THEN 179
				WHEN 'ベトナム' THEN 199
				WHEN 'マレーシア' THEN 224
				WHEN 'ミャンマー' THEN 229
				WHEN 'メキシコ' THEN 230
				WHEN 'モンゴル' THEN 238
				WHEN 'ロシア' THEN 256
				WHEN '韓国' THEN 126
				WHEN '台湾' THEN 127
				WHEN '中国' THEN 133
				WHEN '中国(マカオ)' THEN 216
				WHEN '中国(香港)' THEN 212
				WHEN '日本' THEN 156
				ELSE ''
			END
WHERE system8_1_student_base.`国名` IN ('インドネシア','中国','バングラデシュ','ベトナム','ネパール','韓国','台湾','ミャンマー','パキスタン','マレーシア','モンゴル','ウズベキスタン','チリ','ドイツ','スリランカ','中国(香港)','中国(マカオ)','トルコ','タイ','スリナム','インド','日本','メキシコ','ロシア','アルジェリア','オーストラリア')


-- ------------------------------------------------------------------------------
-- 手順⑥　以下個別UPDATE
-- ------------------------------------------------------------------------------



-- ------------------------------------------------------------------------------
-- check
-- ------------------------------------------------------------------------------
SELECT
`学籍番号` as student_no
,person.id
,1 as type
,`国名` 
,`母国語`
,`生年月日` as birthday
,`性別` as sex
,`未既婚` as married
,`現住所〒` as zipcode
,`現住所住` as address1
,`現住所電話` as phone_no
,`携　帯電話` as mobile_no
,`以前住所〒` as previous_address_zipcode
,`以前住所住` as previous_address1
,`本国氏名1` as name
,`本国氏名書体` as name_font
,`フリガナ名` as name_kana
,`本国氏名2` as name_en
-- ,`本国住所` 
-- ,`本国電話` -- 移行対象外？
-- ,`紹介者名` 
-- ,`本国住所書体` 
-- ,`旅券番号` 
-- ,`旅券有効期限` 
-- ,`入国年月日` 
-- ,`生VISA種類` 
-- ,`登録番号` 
-- ,`VISA更新日(1)` 
-- ,`VISA更新日(2)` 
-- ,`VISA更新日(3)` 
-- ,`VISA更新日(4)` 
-- ,`入管許可番号` 
-- ,`入管許可日` 
-- ,`在留カード番号` 
-- ,`入学日` as start_date
-- ,`卒修予定日` as end_date_intended
-- ,`コース名` as course
-- ,`クラス名` 
-- ,`退学年月日` as end_date1
-- ,`除籍年月日` as end_date2
-- ,`退学除籍理由` as reason_for_exclusion
-- ,`在籍卒業区分` as status_name
-- ,`国民健康保険番号` 
-- ,`障害保険有無` 
-- ,`卒業後進路先` as after_graduate_school
-- ,`紹介者書体` 
-- ,`紹介者名1` 
-- ,`紹介者名2` 
-- ,`紹介者住所1` 
-- ,`紹介者住所2` 
-- ,`紹介者TEL` 
-- ,`紹介者FAX` 
-- ,`J   ﾃｽﾄ` 
-- ,`NAT ﾃｽﾄ` 
-- ,`入学ﾃｽﾄ` 
-- ,`組分ﾃｽﾄ` 
FROM system8_1_student_base KIHON
LEFT JOIN student ON student.student_no=KIHON.`学籍番号`
LEFT JOIN person ON person.id=student.person_id
ORDER BY student.person_id
;

SELECT 
student.student_no
,person.`id`
,`type`
,country.name as `country_id`
,`native_language`
-- ,`native_language_en`
-- ,`parent_person_id`
,`birthday`
,gender.name as `sex_cd`
,marital_status.name as `married_cd`
,`zipcode`
-- ,`address_country_id`
,`address1`
-- ,`address2`
-- ,`address3`
-- ,`address1_en`
-- ,`address2_en`
-- ,`address3_en`
,`phone_no`
-- ,`phone_no2`
,`mobile_no`
,`previous_address_zipcode`
,`previous_address1`
-- ,`previous_address2`
-- ,`previous_address3`
-- ,`mail_address1`
-- ,`mail_address2`
-- ,`sns_account`
-- ,`sns_type`
,`last_name`
,`first_name`
,`name_font`
,`last_name_kana`
,`first_name_kana`
,`last_name_alphabet`
,`first_name_alphabet`
-- ,`legal_registered_last_name`
-- ,`legal_registered_first_name`
-- ,`legal_registered_last_name_kana`
-- ,`legal_registered_first_name_kana`
-- ,`legal_registered_last_name_alphabet`
-- ,`legal_registered_first_name_alphabet`
-- ,`abbreviation_name`
-- ,`last_name_native`
-- ,`first_name_native`
-- ,`photo`
FROM person
INNER JOIN student ON student.person_id=person.id
LEFT JOIN country ON country.id=person.country_id AND country.disable = 0
LEFT JOIN (
	SELECT system_code_detail.code1,system_code_detail.name
	FROM system_code
	INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
	WHERE system_code.disable=0 AND system_code_detail.disable=0 
	AND system_code.column_name='gender'
)gender ON gender.code1=person.sex_cd
LEFT JOIN (
	SELECT system_code_detail.code1,system_code_detail.name
	FROM system_code
	INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
	WHERE system_code.disable=0 AND system_code_detail.disable=0 
	AND system_code.column_name='marital_status'
)marital_status ON marital_status.code1=person.married_cd
WHERE student.student_no NOT IN (1709000,1707097)
ORDER BY person.id
