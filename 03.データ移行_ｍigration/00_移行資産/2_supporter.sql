TRUNCATE TABLE supporter;
INSERT INTO supporter(
person_id
,last_name
,first_name
,address1
,phone_no
,employer_name
,employer_phone_no
,annual_income_amount
,supporter_relationship_cd
)
SELECT
student.person_id
,sys8_imm.name as last_name
,sys8_imm.name as first_name
,sys8_imm.address1
,sys8_imm.phone_no
,sys8_imm.employer_name
,sys8_imm.employer_phone_no
,REPLACE(sys8_imm.annual_income_amount,'円／','') as annual_income_amount
,CASE sys8_imm.supporter_relationship_name
	WHEN '養父' THEN 7
	WHEN '伯父（叔父）伯母（叔母）' THEN 10
	WHEN '不明' THEN 0
	ELSE scd.code1
END as supporter_relationship_cd
FROM system8_1_student_base
INNER JOIN student ON student.student_no=system8_1_student_base.`学籍番号`
LEFT JOIN (
	SELECT
		`学籍番号`
		,`支弁者名1` as name
		,`支弁者住所1` as address1
		,`支弁者電話1` as phone_no
		,`支弁者勤務先1` as employer_name
		,`支弁者勤務先電話1` as employer_phone_no
		,`支弁者年収1` as annual_income_amount
		,`支弁者申請者との関係1` as supporter_relationship_name
		,1 as no
	FROM system8_3_immigration as sup1
	UNION
	SELECT
		`学籍番号`
		,`支弁者名2` as name
		,`支弁者住所2` as address1
		,`支弁者電話2` as phone_no
		,`支弁者勤務先2` as employer_name
		,`支弁者勤務先2電話` as employer_phone_no
		,`支弁者年収2` as annual_income_amount
		,`支弁者申請者との関係2` as supporter_relationship_name
		,2 as no
	FROM system8_3_immigration as sup2
)sys8_imm ON sys8_imm.`学籍番号`=system8_1_student_base.`学籍番号`
LEFT JOIN (
    SELECT
        system_code_detail.code1
        ,system_code_detail.name
    FROM system_code
    INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
    WHERE system_code.disable=0
    AND system_code_detail.disable=0 
    AND system_code.column_name='supporter_relationship_cd'
)scd ON scd.name=sys8_imm.supporter_relationship_name
WHERE true
AND NOT ( true
AND sys8_imm.name =''
AND sys8_imm.address1 =''
AND sys8_imm.phone_no =''
AND sys8_imm.employer_name =''
AND sys8_imm.employer_phone_no =''
AND sys8_imm.supporter_relationship_name ='不明'
)
ORDER BY student.person_id,sys8_imm.no
;

UPDATE supporter
SET 
last_name=SUBSTRING_INDEX(last_name, '　', 1)
,first_name=TRIM(LEADING '　' FROM REPLACE(first_name,SUBSTRING_INDEX(first_name, '　', 1),''))
;

-- ------------------------------------------------------------------------------------------------------------
-- check
-- ------------------------------------------------------------------------------------------------------------
SELECT 
student.student_no as `学籍番号`
,student.person_id as person_id
,`支弁者名1`
,`支弁者住所1`
,`支弁者電話1`
,`支弁者勤務先1`
,`支弁者勤務先電話1`
,`支弁者年収1`
,`支弁者申請者との関係1`
,`支弁者名2`
,`支弁者住所2`
,`支弁者電話2`
,`支弁者勤務先2`
,`支弁者勤務先2電話`
,`支弁者年収2`
,`支弁者申請者との関係2`
FROM student -- 在留資格項目.CSV
LEFT JOIN system8_3_immigration ON student.student_no=system8_3_immigration.`学籍番号`
ORDER BY student.person_id
;

SET @rownum:=0;
SET @person_id:=null;
SELECT
student.person_id
,`支弁者名1`
,`支弁者住所1`
,`支弁者電話1`
,`支弁者勤務先1`
,`支弁者勤務先電話1`
,`支弁者年収1`
,scd1.name as `支弁者申請者との関係1`
,`支弁者名2`
,`支弁者住所2`
,`支弁者電話2`
,`支弁者勤務先2`
,`支弁者勤務先2電話`
,`支弁者年収2`
,scd2.name as `支弁者申請者との関係2`
FROM student
LEFT JOIN (
	SELECT
		person_id
		,max(case rownum when 1 then name else null end) as `支弁者名1`
		,max(case rownum when 1 then address1 else null end) as `支弁者住所1`
		,max(case rownum when 1 then phone_no else null end) as `支弁者電話1`
		,max(case rownum when 1 then employer_name else null end) as `支弁者勤務先1`
		,max(case rownum when 1 then employer_phone_no else null end) as `支弁者勤務先電話1`
		,max(case rownum when 1 then annual_income_amount else null end) as `支弁者年収1`
		,max(case rownum when 1 then supporter_relationship_cd else null end) as `支弁者申請者との関係1`
		,max(case rownum when 2 then name else null end) as `支弁者名2`
		,max(case rownum when 2 then address1 else null end) as `支弁者住所2`
		,max(case rownum when 2 then phone_no else null end) as `支弁者電話2`
		,max(case rownum when 2 then employer_name else null end) as `支弁者勤務先2`
		,max(case rownum when 2 then employer_phone_no else null end) as `支弁者勤務先2電話`
		,max(case rownum when 2 then annual_income_amount else null end) as `支弁者年収2`
		,max(case rownum when 2 then supporter_relationship_cd else null end) as `支弁者申請者との関係2`
	FROM (
		SELECT 
			if(@person_id<> person_id, @rownum:=1, @rownum:=@rownum+1) as rownum
			,@person_id:=person_id as person_id
			,CASE 
					WHEN supporter.last_name='' THEN supporter.first_name
					WHEN supporter.first_name='' THEN supporter.last_name
					ELSE CONCAT(supporter.last_name,'　',supporter.first_name) 
			END as name
			,address1
			,phone_no
			,employer_name
			,employer_phone_no
			,annual_income_amount
			,supporter_relationship_cd
		FROM supporter
	)SUPO
	GROUP BY person_id
)SUPPORTER ON SUPPORTER.person_id=student.person_id
LEFT JOIN (
    SELECT
        system_code_detail.code1
        ,system_code_detail.name
    FROM system_code
    INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
    WHERE system_code.disable=0
    AND system_code_detail.disable=0 
    AND system_code.column_name='supporter_relationship_cd'
)scd1 ON scd1.code1=SUPPORTER.`支弁者申請者との関係1`
LEFT JOIN (
    SELECT
        system_code_detail.code1
        ,system_code_detail.name
    FROM system_code
    INNER JOIN system_code_detail ON system_code_detail.system_code_id=system_code.id
    WHERE system_code.disable=0
    AND system_code_detail.disable=0 
    AND system_code.column_name='supporter_relationship_cd'
)scd2 ON scd2.code1=SUPPORTER.`支弁者申請者との関係2`
ORDER BY student.person_id
;